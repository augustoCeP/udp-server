import java.io.Serializable;
import java.util.Arrays;

public class Packet implements Serializable {

    private int id;

    private byte[] data;

    private boolean acknowledged;

    public Packet(int id, byte[] data) {
        this.id = id;
        this.data = data;
        this.acknowledged = false;
    }

    @Override
    public String toString() {
        return "Packet{"+new String(data) +"}" ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }
}

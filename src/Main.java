
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static final int packageSize = 200;
    private static int packageNum;
    private static int packagePerS;
    private static int windowSize;
    private static ArrayList<DatagramPacket> window = new ArrayList();
    private static ArrayList<Packet> ackPackets = new ArrayList();

    private static int unconfirmedPackages;
    private static int lastPosition = 0;
    private static DatagramPacket[] packets;
    static DatagramSocket serverSocket;
    static int count = 0;
    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);

        System.out.println("Informe a quantidade de pacotes a ser enviado");
        packageNum = s.nextInt();

        System.out.println("informe a quantidade de pacotes a serem enviados por segundo");
        packagePerS = s.nextInt();

        System.out.println("informe a quantidade de pacotes a serem enviados antes da confirmacao");
        windowSize = s.nextInt();

        int porta = 9876;
        InetAddress clientIP = InetAddress.getByName("localhost");
        int clientPort = 9877;

         serverSocket = new DatagramSocket(porta);

        byte[] receiveData = new byte[1024];

        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        createReceiveThread(serverSocket, receivePacket);

        packets = new DatagramPacket[packageNum];

        for (int i = 0; i < packageNum; i++) {

            byte[] message = Serializer.toBytes(new Packet(i, ("Pacote " + i).getBytes()));
            packets[i] = new DatagramPacket(message, message.length, clientIP, clientPort);
        }

        for (int i = 0; i < windowSize; i++) {
            window.add(packets[i]);
        }
        unconfirmedPackages = windowSize;
        lastPosition = windowSize;
        while (ackPackets.size() != packageNum) {

            for (int i = 0; i < window.size(); i++) {
                if (i % packagePerS == 0) {
                    Thread.currentThread().sleep(1000);
                //count = 0;
                }
                if (!window.isEmpty()) {
                    serverSocket.send(window.get(i));
                    //count++;
                }
            }
            System.out.println("Confirmados \t não confirmados \t não enviados \n " + ackPackets.size() + "\t\t\t\t\t" + unconfirmedPackages + "\t\t\t\t\t" + (packageNum - (ackPackets.size() + unconfirmedPackages)));
        }


    }

    public static void createReceiveThread(DatagramSocket socket, DatagramPacket receivePacket) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        socket.receive(receivePacket);
                        changeReceived((Packet) Serializer.toObject(receivePacket.getData()));
                    }


                } catch (Exception e) {

                }
            }
        }).start();
    }

    public static void changeReceived(Packet receivedPacket) {
        try {
            for (DatagramPacket datagramPacket : window) {
                Packet packet = (Packet) Serializer.toObject(datagramPacket.getData());

                if (packet.getId() == receivedPacket.getId()) {
                    window.remove(datagramPacket);
                    packet.setAcknowledged(true);
                    ackPackets.add(packet);
                   System.out.println("Recebeu: " + packet);
                    unconfirmedPackages--;
                    window.add(packets[lastPosition]);
                    //if (count % packagePerS == 0) {
                      //  Thread.currentThread().sleep(1000);
                    //    count = 0;
                  //  }
                    serverSocket.send(packets[lastPosition]);
                    count++;
                    unconfirmedPackages++;
                    lastPosition++;


                    return;
                }
            }
        } catch (Exception e) {

        }
    }


}
